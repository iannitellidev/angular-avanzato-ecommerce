import { createReducer, on } from '@ngrx/store';

import { addToCart, removeFromCart, resetCart } from './cart.actions';

export const initialState = [];

const _cartReducer = createReducer(
  initialState,
  on(addToCart, (state, element) => [...state, element]),
  on(removeFromCart, (state, elementToRemove) => {
    console.log('elementToRemove', elementToRemove);
    console.log('state', state);
    const indexToRemove = state.findIndex(el => el.id === elementToRemove.id);
    console.log('indexToRemove', indexToRemove);
    return [...state.slice(0, indexToRemove), ...state.slice(indexToRemove + 1)];
  }
  ),
  on(resetCart, () => initialState));

export function cartReducer(state, action): any {
  return _cartReducer(state, action);
}
