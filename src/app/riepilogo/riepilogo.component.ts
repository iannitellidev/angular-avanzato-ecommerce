import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-riepilogo',
  templateUrl: './riepilogo.component.html',
  styleUrls: ['./riepilogo.component.css']
})
export class RiepilogoComponent implements OnInit {

  count: Observable<number>;

  constructor(private store: Store<{ count: number }>) {
    this.count = this.store.select('count');
  }

  ngOnInit(): void {
  }

}
