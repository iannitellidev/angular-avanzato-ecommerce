import { Component, OnInit } from '@angular/core';
import { Category } from '../model/category';
import { AjaxService } from '../services/ajax.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { addToCart, removeFromCart } from '../cart.actions';
import { aggiungiPreferiti, rimuoviPreferiti } from '../preferiti.action';
import { Product } from '../model/product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  cart: Observable<any[]>;
  products: Product[];
  productsToShow: Product[];
  search='';
  searchPriceTo=2000;
  searchPriceFrom=0;

  constructor(
    private ajaxService: AjaxService,
    private store: Store<{ cart: any[] }>)
    {
    this.cart = store.select('cart');
  }

  ngOnInit(): void {
    this.ajaxService.getProductsList().subscribe((res) => {
      this.products = res;
      this.productsToShow = res;
      console.log(res);
      console.log('products', this.products);
    });
  }

  aggiungiAlCarrello(product): void {
    console.log('aggiungiAlCarrello', product);
    this.store.dispatch(addToCart(product));
  }
  eliminaDaCarrello(posizione): void {
    console.log('elimina da carrello', posizione);
    this.store.dispatch(removeFromCart(posizione));
  }

  aggiungiAPreferiti(product): void {
    console.log('aggiungi a preferiti', product);
    this.store.dispatch(aggiungiPreferiti(product));
  }
  avviaRicerca(): void {
    this.productsToShow = this.products.filter(
      product =>
      //{
/*         if(!this.searchPriceTo){

          this.searchPriceTo = 3000;
        } */

        (
        ((product.name.toLocaleLowerCase().includes(this.search.toLocaleLowerCase()))
          || (product.description.toLocaleLowerCase().includes(this.search.toLocaleLowerCase()))
        )
        &&
        ( product.prezzo > this.searchPriceFrom && product.prezzo < this.searchPriceTo)

        )
     //   return this.productsToShow;
     // }
    );
    console.log('productsToShow', this.productsToShow);
  }

  formatLabel(value: any) {
/*     if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    } */

    return parseInt(value);
  }

}
