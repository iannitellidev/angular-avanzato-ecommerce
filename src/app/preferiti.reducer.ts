import { createReducer, on } from '@ngrx/store';

import { aggiungiPreferiti, rimuoviPreferiti, resetPreferiti } from './preferiti.action';

export const initialState = [];

const _preferitiReducer = createReducer(
  initialState,
  on(aggiungiPreferiti, (state, element) => [...state, element]),
  on(rimuoviPreferiti, (state, elementToRemove) => {
    console.log('elementToRemove', elementToRemove);
    console.log('state', state);
    const indexToRemove = state.findIndex(el => el.id === elementToRemove.id);
    console.log('indexToRemove', indexToRemove);
    return [...state.slice(0, indexToRemove), ...state.slice(indexToRemove + 1)];
  }
  ),
  on(resetPreferiti, () => initialState));

export function preferitiReducer(state, action): any {
  return _preferitiReducer(state, action);
}
