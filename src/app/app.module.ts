import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './counte.reducer';
import { RiepilogoComponent } from './riepilogo/riepilogo.component';
import { cartReducer } from './cart.reducer';
import { preferitiReducer } from './preferiti.reducer';
import { PreferitiComponent } from './preferiti/preferiti.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import { MyWrapperModule } from 'my-wrapper';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CounterComponent,
    RiepilogoComponent,
    PreferitiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({
      count: counterReducer,
      cart: cartReducer,
      preferiti: preferitiReducer
    }),
    BrowserAnimationsModule,
    MatSliderModule,
    MatInputModule,
    MyWrapperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
