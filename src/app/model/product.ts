import { Category } from './category';
import { Immagine } from './immagine';

export interface Product {
  id: number;
  name: string;
  description: string;
  category: Category;
  created_at: Date;
  updated_at: Date;
  image: Immagine;
  prezzo: number;
}
