import { Immagine } from './immagine';
import { Product } from './product';

export interface Category {
  id: number;
  name: string;
  description: string;
  created_at: Date;
  updated_at: Date;
  attachment: Immagine;
  prodottis: Product[];
}
