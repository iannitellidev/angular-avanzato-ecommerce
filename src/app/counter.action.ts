import {createAction} from '@ngrx/store';

export const increment = createAction('INCREMENTA_CONTO');

export const decrement = createAction('DECREMENTA_CONTO');

export const reset = createAction('RESET_CONTO');
