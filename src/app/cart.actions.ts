import {createAction, props} from '@ngrx/store';

export const addToCart = createAction('ADD_TO_CART', props<any>());
export const removeFromCart = createAction('REMOVE_FROM_CART', props<any>());
export const resetCart = createAction('RESET_CART');
