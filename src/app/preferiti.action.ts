import {createAction, props} from '@ngrx/store';

export const aggiungiPreferiti = createAction('ADD_A_PREFERITI', props<any>());
export const rimuoviPreferiti = createAction('RIMUOVI_PREFERITI', props<any>());
export const resetPreferiti = createAction('RESET_PREFERITI');
