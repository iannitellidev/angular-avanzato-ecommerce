import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { rimuoviPreferiti } from '../preferiti.action';


@Component({
  selector: 'app-preferiti',
  templateUrl: './preferiti.component.html',
  styleUrls: ['./preferiti.component.css']
})
export class PreferitiComponent implements OnInit {
  preferiti: Observable<any[]>;

  constructor(private store: Store<{ preferiti: any[] }>)
  {
    this.preferiti = store.select('preferiti');
  }



  eliminaDaPreferiti(posizione): void {
    console.log('elimina da carrello', posizione);
    this.store.dispatch(rimuoviPreferiti(posizione));
  }


  ngOnInit(): void {

    console.log("ngOnInit");
  }


}
