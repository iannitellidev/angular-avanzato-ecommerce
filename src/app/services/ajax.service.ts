import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Category } from '../model/category';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  BASE_URL = 'http://localhost:1337';

  constructor(private http: HttpClient) { }

  getCategoriesList(): Observable<Category[]> {
    return this.http.get<Category[]>(`${this.BASE_URL}/categories`);
  }

  getCategoryById(id: number): Observable<Category> {
    return this.http.get<Category>(`${this.BASE_URL}/categories/${id}`);
  }

  getProductsList(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.BASE_URL}/prodottis`);
  }

  getProductById(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.BASE_URL}/prodottis/${id}`);
  }

}


